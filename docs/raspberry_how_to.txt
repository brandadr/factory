##############
#   Setup    #
##############

# 0. OS auf Raspberry laden mit Raspberry Pi Imager( https://www.raspberrypi.org/software)

# 1. Raspberry mit AngryIp-Scanner suchen (https://angryip.org/download/#windows)

# 2. Raspberry konfigurieren 
raspi-config

# 3. WLAN-Settings 
# Entweder über Settings-Konfiguration oder manuell https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

# 4. WLAN-Settings überprüfen
iwconfig


##############################
#   Ports konfigurieren
##############################

# Port 17 einschalten
echo 17 > /sys/class/gpio/export

# Port 17 als Eingang oder Ausgang
echo in > /sys/class/gpio/gpio17/direction
echo out > /sys/class/gpio/gpio17/direction

# Port 17 Wert setzen
echo 1 > /sys/class/gpio/gpio17/value
echo 0 > /sys/class/gpio/gpio17/value

#Port 17 Zustand lesen
cat /sys/class/gpio/gpio17/value

#Port 17 deaktivieren
echo 17 > /sys/class/gpio/unexport

##############################
#   Bluetooth Settings 
##############################

# Link (https://www.raspberry-pi-geek.de/ausgaben/rpg/2016/04/bluetooth-auf-dem-raspberry-pi-3-einrichten/2/)

#Bluetoothmanager öffnen. Mit sudo starten sonst wird Bluetooth Agent nicht gefunden
sudo bluetoothctl

#Alternative zum Suchen von Bluetoothgeräten
sudo hcitool lescan

#Python Lib (https://tutorials-raspberrypi.de/raspberry-pi-miflora-xiaomi-pflanzensensor-openhab/)