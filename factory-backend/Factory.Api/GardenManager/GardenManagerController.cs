﻿using Factory.Core.GardenManager.Dtos;
using Factory.Core.GardenManager.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Api.DeviceManager
{
   [Route("api/[controller]")]
   [ApiController]
   public class GardenManagerController : ControllerBase
   {
      private readonly IGardenManagerService gardenManagerService;

      public GardenManagerController(IGardenManagerService gardenManagerService)
      {
         this.gardenManagerService = gardenManagerService;
      }


      [HttpGet("devices/information/{id}")]
      public async Task<DeviceInformationDto> ReadDeviceInformationAsync(int id)
      {
         return await this.gardenManagerService.ReadDeviceInformationAsync(id);
      }


      [HttpGet("devices/information")]
      public async Task<List<DeviceInformationDto>> ReadDeviceInformationAsync()
      {
         return await this.gardenManagerService.ReadDeviceInformationAsync();
      }

      [HttpGet("devices")]
      public async Task<List<DeviceDto>> GetDevices()
      {
         return await this.gardenManagerService.GetDevicesAsync();
      }

      [HttpPut("devices/update")]
      public async Task WriteDeviceInformation([FromBody] DeviceCommandDto request)
      {
         await this.gardenManagerService.WriteDeviceInformation(request);
      }

      [HttpGet("logs")]
      public async Task<List<string>> GetLogs()
      {
         return await this.gardenManagerService.GetLogsAsync();
      }
   }
}
