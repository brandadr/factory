﻿using Factory.Core.Interfaces.MessagingAssistant;
using Factory.Core.MessagingAssistant.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Api.MessagingAssistant
{
   [Route("api/[controller]")]
   [ApiController]
   public class MessagingAssistantController : ControllerBase
   {
      private readonly IMessagingAssistantService messagingAssistantService;

      public MessagingAssistantController(IMessagingAssistantService messagingAssistantService)
      {
         this.messagingAssistantService = messagingAssistantService;
      }

      [HttpGet]
      public List<string> Get()
      {
         return this.messagingAssistantService.GetMessageHistory();
      }

      [HttpPost]
      public async Task Post([FromBody] MessageDto messageDto)
      {
         await this.messagingAssistantService.SendMessageAsync(messageDto);
      }
   }
}
