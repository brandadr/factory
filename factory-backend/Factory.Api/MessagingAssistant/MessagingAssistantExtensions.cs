﻿using Microsoft.Extensions.DependencyInjection;

namespace Factory.Api.MessagingAssistant
{
   public static class MessagingAssistantExtensions
   {
      public static IServiceCollection AddMessagingAssistantApi(this IServiceCollection services)
      {
         services.AddTransient<MessagingAssistantController>();
         return services;
      }
   }
}
