﻿using Factory.Core.RunningEvents.Dtos;
using Factory.Core.RunningEvents.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Api.RunningEvents
{
    [Route("api/[controller]")]
    [ApiController]
    public class RunningEventController : ControllerBase
    {
        private readonly IRunningEventService runningEventService;

        public RunningEventController(IRunningEventService runningEventService)
        {
            this.runningEventService = runningEventService;
        }

        [HttpGet("state")]
        public async Task<string> GetState()
        {
            return await Task.FromResult("running");
        }

        [HttpGet]
        public async Task<IReadOnlyList<RunningEventDto>> Get()
        {
            return await this.runningEventService.GetRunningEventsAsync();
        }

        [HttpGet("{id}")]
        public async Task<RunningEventDto> Get(int id)
        {
            return await this.runningEventService.GetRunningEventAsync(id);
        }

        [HttpPost]
        public async Task Post([FromBody] RunningEventDto runningEvent)
        {
            await this.runningEventService.SaveRunningEventAsync(runningEvent);
        }

        [HttpPut]
        public async Task Put([FromBody] RunningEventDto runningEvent)
        {
            await this.runningEventService.SaveRunningEventAsync(runningEvent);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await this.runningEventService.DeleteRunningEventAsync(id);
        }

        [HttpGet("strava/access-tokens/{code}")]
        public async Task GenerateStravaAccessTokens(string code)
        {
            await this.runningEventService.GenerateStravaAccessTokens(code);
        }

        [HttpGet("strava/activities")]
        public async Task<StravaActivitiesDto> GetActivities()
        {
            return await this.runningEventService.GetActivities();
        }

        [HttpGet("strava/is-token-expired")]
        public async Task<bool> IsTokenExpired()
        {
            return await this.runningEventService.IsTokenExpired();
        }
    }
}
