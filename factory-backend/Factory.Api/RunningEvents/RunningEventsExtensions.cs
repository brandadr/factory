﻿using Factory.Api.RunningEvents;
using Microsoft.Extensions.DependencyInjection;

namespace Factory.Api
{
   public static class RunningEventsExtensions
   {
      public static IServiceCollection AddRunningEventsApi(this IServiceCollection services)
      {
         services.AddTransient<RunningEventController>();
         return services;
      }
   }
}