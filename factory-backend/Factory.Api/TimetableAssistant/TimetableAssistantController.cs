﻿using Factory.Core.TimetableAssistant.Dtos;
using Factory.Core.TimetableAssistant.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Api.TimetableAssistant
{
   [Route("api/[controller]")]
   [ApiController]
   public class TimetableAssistantController : ControllerBase
   {
      private readonly ITimetableAssistantService timetableAssistantService;

      public TimetableAssistantController(ITimetableAssistantService timetableAssistantService)
      {
         this.timetableAssistantService = timetableAssistantService;
      }

      [HttpGet]
      public async Task<IReadOnlyList<TimetableConnectionDto>> Get()
      {
         return await this.timetableAssistantService.GetTimetableConnectionsAsync();
      }

      [HttpGet("{id}")]
      public async Task<TimetableConnectionDto> Get(int id)
      {
         return await this.timetableAssistantService.GetTimetableConnectionAsync(id);
      }

      [HttpPost]
      public async Task Post([FromBody] TimetableConnectionDto timetableConnection)
      {
         await this.timetableAssistantService.SaveTimetableConnectionAsync(timetableConnection);
      }

      [HttpPut]
      public async Task Put([FromBody] TimetableConnectionDto timetableConnection)
      {
         await this.timetableAssistantService.SaveTimetableConnectionAsync(timetableConnection);
      }

      [HttpDelete("{id}")]
      public async Task Delete(int id)
      {
         await this.timetableAssistantService.DeleteTimetableConnectionAsync(id);
      }
   }
}
