﻿using Microsoft.Extensions.DependencyInjection;

namespace Factory.Api.TimetableAssistant
{
   public static class TimetableAssistantExtensions
   {
      public static IServiceCollection AddTimetableAssistantApi(this IServiceCollection services)
      {
         services.AddTransient<TimetableAssistantController>();
         return services;
      }
   }
}
