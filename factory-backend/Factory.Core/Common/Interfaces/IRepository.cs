﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.Common.Interfaces
{
   public interface IRepository<T>
   {
      Task<IReadOnlyList<T>> GetAllAsync();

      Task SaveAsync(T runningEvent);

      Task DeleteAsync(int eventId);

      Task<T> GetByIdAsync(int eventId);

      Task<int> SaveChangesAsync();
   }
}
