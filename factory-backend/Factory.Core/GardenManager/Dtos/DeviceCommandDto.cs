﻿namespace Factory.Core.GardenManager.Dtos
{
   public class DeviceCommandDto
   {
      public int DeviceId { get; set; }

      public string Command { get; set; }
   }
}
