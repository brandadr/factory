﻿namespace Factory.Core.GardenManager.Dtos
{
   public class DeviceDto
   {
      public int Id { get; set; }
      public string Type { get; set; }
      public string Name { get; set; }
      public string Description { get; set; }
      public string IpAddress { get; set; }
   }
}
