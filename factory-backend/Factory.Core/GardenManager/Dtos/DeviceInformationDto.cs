﻿using System.Collections.Generic;

namespace Factory.Core.GardenManager.Dtos
{
   public class DeviceInformationDto
   {
      public int DeviceId { get; set; }
      public List<DeviceCommandDto> Commands { get; set; }
      public string State { get; set; }
      public string NetworkState { get; set; }
   }
}
