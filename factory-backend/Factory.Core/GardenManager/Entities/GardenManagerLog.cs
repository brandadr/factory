﻿using Factory.Core.GardenManager.Interfaces;

namespace Factory.Core.GardenManager.Entities
{
   public class GardenManagerLog : IGardenManagerLog
   {
      public int Id { get; set; }
      public string Message { get; set; }
      public System.DateTime EventDate { get; set; }
   }
}
