﻿namespace Factory.Core.GardenManager.Entities
{
   public class ShellyConfig
   {
      public string Shelly1Ip { get; set; }

      public string Shelly2Ip { get; set; }
   }
}
