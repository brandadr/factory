﻿using Factory.Core.GardenManager.Interfaces;
using Factory.Core.GardenManager.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Factory.Core.GardenManager
{
   public static class GardenManagerExtensions
   {
      public static IServiceCollection AddGardenManagerCore(this IServiceCollection services)
      {
         services.AddTransient<IGardenManagerService, GardenManagerService>();
         return services;
      }
   }
}
