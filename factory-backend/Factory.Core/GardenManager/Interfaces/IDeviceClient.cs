﻿using System.Threading.Tasks;

namespace Factory.Core.GardenManager.Interfaces
{
   public interface IDeviceClient
   {
      string IpAddress { get; }

      Task<bool> IsOnlineAsync();

      Task<bool> GetGpioStateAsync(int relayId);

      Task SetGpio(int relayId, bool isRelayOn);
   }
}
