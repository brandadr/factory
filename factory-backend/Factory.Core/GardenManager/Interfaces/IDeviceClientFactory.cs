﻿namespace Factory.Core.GardenManager.Interfaces
{
   public interface IDeviceClientFactory
   {
      IRaspberryDeviceClient CreateRaspberryPiClient(string ipAddress, string username = null, string password = null);

      IShellyDeviceClient CreateShellyClient(string ipAddress, string username = null, string password = null);
   }
}
