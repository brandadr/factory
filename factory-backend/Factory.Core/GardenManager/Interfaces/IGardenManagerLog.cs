﻿using System;

namespace Factory.Core.GardenManager.Interfaces
{
   public interface IGardenManagerLog
   {
      int Id { get; set; }

      string Message { get; set; }

      DateTime EventDate { get; set; }
   }
}
