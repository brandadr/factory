﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.GardenManager.Interfaces
{
   public interface IGardenManagerRepository
   {
      Task<IReadOnlyList<IGardenManagerLog>> GetAllAsync();

      Task AddOrUpdate(IGardenManagerLog log);

      Task DeleteAsync(int logId);

      Task<IGardenManagerLog> GetByIdAsync(int logId);

      Task<int> SaveChangesAsync();
   }
}
