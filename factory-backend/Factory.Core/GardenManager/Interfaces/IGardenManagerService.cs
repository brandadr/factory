﻿using Factory.Core.GardenManager.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.GardenManager.Interfaces
{
   public interface IGardenManagerService
   {
      Task<DeviceInformationDto> ReadDeviceInformationAsync(int id);

      Task<List<DeviceInformationDto>> ReadDeviceInformationAsync();

      Task<List<DeviceDto>> GetDevicesAsync();

      Task WriteDeviceInformation(DeviceCommandDto deviceCommand);

      Task<List<string>> GetLogsAsync();
   }
}
