﻿namespace Factory.Core.GardenManager.Interfaces
{
   public interface IRaspberryDeviceClient : IDeviceClient
   {
      public void ActivateGpio(int gpioId);

      public void DeactivateGpio(int gpioId);

      public void SetGpioAsOutput(int gpioId);

      public void SetGpioAsInput(int gpioId);

      string ExecuteCommand(string command);
   }
}
