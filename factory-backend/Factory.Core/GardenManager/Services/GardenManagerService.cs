﻿using Factory.Core.GardenManager.Dtos;
using Factory.Core.GardenManager.Entities;
using Factory.Core.GardenManager.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Factory.Core.GardenManager.Services
{
   internal class GardenManagerService : IGardenManagerService
   {
      private readonly string shellyPumpIp;
      private readonly string shellyLightIp;
      private readonly string raspberryPiIp;

      private readonly IOptions<RaspberryPiConfig> raspberryPiOptions;
      private readonly IOptions<ShellyConfig> shellyOptions;

      private readonly IGardenManagerRepository gardenManagerRepository;
      private readonly IDeviceClientFactory deviceClientFactory;

      private List<DeviceDto> availableDevices = new List<DeviceDto>();

      public GardenManagerService(
         IGardenManagerRepository gardenManagerRepository,
         IDeviceClientFactory deviceClientFactory,
         IOptions<ShellyConfig> shellyOptions,
         IOptions<RaspberryPiConfig> raspberryPiOptions)
      {
         this.shellyOptions = shellyOptions;
         this.raspberryPiOptions = raspberryPiOptions;
         this.gardenManagerRepository = gardenManagerRepository;
         this.shellyPumpIp = this.shellyOptions.Value.Shelly1Ip;
         this.shellyLightIp = this.shellyOptions.Value.Shelly2Ip;
         this.raspberryPiIp = this.raspberryPiOptions.Value.RaspberryPiIp;
         this.deviceClientFactory = deviceClientFactory;
         this.availableDevices = this.InitDevices();
      }

      public async Task<DeviceInformationDto> ReadDeviceInformationAsync(int id)
      {
         var devices = new List<DeviceInformationDto>();
         var device = this.availableDevices.FirstOrDefault(d => d.Id == id);

         if (device.IpAddress == this.shellyPumpIp)
         {
            var shellyClientPump = this.deviceClientFactory.CreateShellyClient(this.shellyPumpIp);
            await BuildShellyInformation(devices, shellyClientPump, device);
         }
         else if (device.IpAddress == this.shellyLightIp)
         {
            var shellyClientLight = this.deviceClientFactory.CreateShellyClient(this.shellyLightIp);
            await BuildShellyInformation(devices, shellyClientLight, device);
         }
         else if (device.IpAddress == this.raspberryPiIp)
         {
            var raspberryClient = this.deviceClientFactory.CreateRaspberryPiClient(this.raspberryPiIp, this.raspberryPiOptions.Value.UserName, this.raspberryPiOptions.Value.Password);
            await BuildRaspberryInformation(devices, raspberryClient, device);
         }

         return devices.FirstOrDefault();
      }

      /// <summary>
      /// Todo: Enums erstellen!
      /// </summary>
      /// <returns></returns>
      public async Task<List<DeviceInformationDto>> ReadDeviceInformationAsync()
      {
         var devices = new List<DeviceInformationDto>();
         var shellyClientPump = this.deviceClientFactory.CreateShellyClient(this.shellyPumpIp);
         var shellyClientLight = this.deviceClientFactory.CreateShellyClient(this.shellyLightIp);
         var raspberryClient = this.deviceClientFactory.CreateRaspberryPiClient(this.raspberryPiIp, this.raspberryPiOptions.Value.UserName, this.raspberryPiOptions.Value.Password);

         foreach (var device in this.availableDevices)
         {
            await BuildShellyInformation(devices, shellyClientPump, device);
            await BuildShellyInformation(devices, shellyClientLight, device);
            await BuildRaspberryInformation(devices, raspberryClient, device);
         }

         return devices;
      }

      public async Task WriteDeviceInformation(DeviceCommandDto deviceCommand)
      {
         var isSuccessful = true;
         var deviceToWrite = this.availableDevices.FirstOrDefault(d => d.Id == deviceCommand.DeviceId);

         try
         {
            if (deviceToWrite.Type == "Shelly")
            {
               var shellyClient = this.deviceClientFactory.CreateShellyClient(deviceToWrite.IpAddress);
               await shellyClient.SetGpio(0, deviceCommand.Command == "ON");
            }

            if (deviceToWrite.Type == "Sensor")
            {
               //var raspberryDeviceClient = this.deviceClientFactory.CreateRaspberryPiClient(device.IpAddress, this.raspberryPiOptions.Value.UserName, this.raspberryPiOptions.Value.Password);
               //await raspberryDeviceClient.GetGpioState(17);
            }
         }
         catch
         {
            isSuccessful = false;
         }
         finally
         {
            await this.gardenManagerRepository.AddOrUpdate(new GardenManagerLog()
            {
               Message = $"{DateTime.Now}; Method; {nameof(WriteDeviceInformation)}; IpAddress; {deviceToWrite.IpAddress}; Command; {deviceCommand.Command}; Successful; {isSuccessful}",
               EventDate = DateTime.Now
            });
            await this.gardenManagerRepository.SaveChangesAsync();
         }
      }

      public async Task<List<DeviceDto>> GetDevicesAsync()
      {
         return await Task.FromResult(this.availableDevices);
      }

      public async Task<List<string>> GetLogsAsync()
      {
         var logs = await this.gardenManagerRepository.GetAllAsync();
         return logs.OrderByDescending(l => l.EventDate).Select(l => $"Id: {l.Id} Date: {l.EventDate} Content: {l.Message}").ToList();
      }

      private List<DeviceDto> InitDevices()
      {
         var devices = new List<DeviceDto>();

         devices.Add(new DeviceDto()
         {
            Id = 0,
            Name = "Pumpe",
            Type = "Shelly",
            IpAddress = this.shellyPumpIp,
            Description = "Pumpe zur Bewässerung vom Hochbeet"
         });

         devices.Add(new DeviceDto()
         {
            Id = 1,
            Name = "Lampe",
            Type = "Shelly",
            IpAddress = this.shellyLightIp,
            Description = "Lampe zur Beleuchtung vom Hochbeet"
         });

         devices.Add(new DeviceDto()
         {
            Id = 2,
            Name = "Füllstandssensor",
            Type = "Sensor",
            IpAddress = this.raspberryPiIp,
            Description = "Sensoren zur Anzeige vom Füllstand"
         });

         devices.Add(new DeviceDto()
         {
            Id = 3,
            Name = "Sensoren Beet 1",
            Type = "Sensor",
            IpAddress = this.raspberryPiIp,
            Description = "Sensoren zur Anzeige vom Zustand im Beet"
         });

         devices.Add(new DeviceDto()
         {
            Id = 4,
            Name = "Sensoren Beet 2",
            Type = "Sensor",
            IpAddress = this.raspberryPiIp,
            Description = "Sensoren zur Anzeige vom Zustand im Beet"
         });

         return devices;
      }

      private async Task BuildRaspberryInformation(List<DeviceInformationDto> devices, IRaspberryDeviceClient raspberryClient, DeviceDto device)
      {
         var deviceToSetup = this.availableDevices.FirstOrDefault(d => d.Id == device.Id);

         if (device.Id == deviceToSetup.Id && raspberryClient.IpAddress == deviceToSetup.IpAddress)
         {
            var state = "offline";
            var isOnline = true;// await raspberryClient.IsOnlineAsync();
            var availableCommands = new List<DeviceCommandDto>();

            if (device.Id == 2)
            {
               if (isOnline)
               {
                  raspberryClient.ActivateGpio(17);
                  raspberryClient.SetGpioAsInput(17);
                  var sensorTop = (await raspberryClient.GetGpioStateAsync(17)) ? "ON" : "OFF";

                  raspberryClient.ActivateGpio(16);
                  raspberryClient.SetGpioAsInput(16);
                  var sensorBottom = (await raspberryClient.GetGpioStateAsync(16)) ? "ON" : "OFF";


                  // Mögliche Zustände
                  if (sensorTop == "ON" && sensorBottom == "ON")
                  {
                     state = "Wasserstand hoch";
                  }
                  else if (sensorTop == "OFF" && sensorBottom == "ON")
                  {
                     state = "Wasserstand mittel";
                  }
                  else if (sensorTop == "OFF" && sensorBottom == "OFF")
                  {
                     state = "Wasserstand tief";
                  }
                  else
                  {
                     state = $"Unbekannter Zustand; {nameof(sensorBottom)}; {sensorBottom}; {nameof(sensorTop)}; {sensorTop}";
                  }

                  availableCommands = new List<DeviceCommandDto>() { new DeviceCommandDto() { DeviceId = device.Id, Command = "READ" } };
               }
            }

            if (device.Id == 3)
            {
               if (isOnline)
               {
                  var sensorData = raspberryClient.ExecuteCommand(@"sudo python3 /home/pi/flowercare/flowercare_sensor1.py");
                  state = sensorData;
                  availableCommands = new List<DeviceCommandDto>() { new DeviceCommandDto() { DeviceId = device.Id, Command = "READ" } };
               }
            }

            if (device.Id == 4)
            {
               if (isOnline)
               {
                  var sensorData = raspberryClient.ExecuteCommand(@"sudo python3 /home/pi/flowercare/flowercare_sensor2.py");
                  state = sensorData;
                  availableCommands = new List<DeviceCommandDto>() { new DeviceCommandDto() { DeviceId = device.Id, Command = "READ" } };
               }
            }

            devices.Add(new DeviceInformationDto()
            {
               DeviceId = deviceToSetup.Id,
               State = state,
               Commands = availableCommands
            });
         }
      }

      private async Task BuildShellyInformation(List<DeviceInformationDto> devices, IShellyDeviceClient shellyClient, DeviceDto device)
      {
         var deviceToSetup = this.availableDevices.FirstOrDefault(d => d.Id == device.Id);
         var shellyCommands = new List<DeviceCommandDto>()
         {
            new DeviceCommandDto()
            {
               Command = "ON",
               DeviceId = deviceToSetup.Id
            },
            new DeviceCommandDto()
            {
               Command = "OFF",
               DeviceId = deviceToSetup.Id
            }
         };

         if (device.Id == deviceToSetup.Id && shellyClient.IpAddress == deviceToSetup.IpAddress)
         {
            var state = "offline";
            var availableCommands = new List<DeviceCommandDto>();
            var isOnline = await shellyClient.IsOnlineAsync();

            if (isOnline)
            {
               state = (await shellyClient.GetGpioStateAsync(0)) ? "ON" : "OFF";
               availableCommands = shellyCommands.Where(c => c.Command != state).ToList();
            }

            devices.Add(new DeviceInformationDto()
            {
               DeviceId = deviceToSetup.Id,
               State = state,
               Commands = availableCommands
            });
         }
      }
   }
}
