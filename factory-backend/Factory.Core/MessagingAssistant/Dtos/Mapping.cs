﻿using Factory.Core.MessagingAssistant.Entities;

namespace Factory.Core.MessagingAssistant.Dtos
{
   public static class Mapping
   {
      public static Message FromDto(this MessageDto message)
      {
         return new Message()
         {
            Content = message.Content,
            QueueName = message.QueueName
         };
      }
   }
}
