﻿namespace Factory.Core.MessagingAssistant.Dtos
{
   public class MessageDto
   {
      public string Content { get; set; }

      public string QueueName { get; set; }
   }
}
