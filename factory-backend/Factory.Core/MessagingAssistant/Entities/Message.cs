﻿namespace Factory.Core.MessagingAssistant.Entities
{
   public class Message
   {
      public string Content { get; set; }

      public string QueueName { get; set; }
   }
}
