﻿namespace Factory.Core.MessagingAssistant.Interfaces
{
   public interface IBotService
   {
      void StartReceiving();

      void StopReceiving();
   }
}
