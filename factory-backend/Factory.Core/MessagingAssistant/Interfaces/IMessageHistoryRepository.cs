﻿using System.Collections.Generic;

namespace Factory.Core.MessagingAssistant.Interfaces
{
   public interface IMessageHistoryRepository
   {
      void Add(string message);

      List<string> GetAll();
   }
}
