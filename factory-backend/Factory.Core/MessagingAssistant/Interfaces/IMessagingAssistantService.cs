﻿using Factory.Core.MessagingAssistant.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.Interfaces.MessagingAssistant
{
   public interface IMessagingAssistantService
   {
      public Task SendMessageAsync(MessageDto message);

      public List<string> GetMessageHistory();
   }
}
