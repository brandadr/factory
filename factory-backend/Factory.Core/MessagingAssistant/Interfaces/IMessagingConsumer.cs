﻿using System.Collections.Generic;

namespace Factory.Core.MessagingAssistant.Interfaces
{
   public interface IMessagingConsumer
   {
      void StartConsuming(string queueName);

      List<string> GetMessageHistory();
   }
}
