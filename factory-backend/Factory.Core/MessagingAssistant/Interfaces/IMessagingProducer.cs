﻿namespace Factory.Core.Interfaces.MessagingAssistant
{
   public interface IMessagingProducer
   {
      void PublishMessage(string message, string queueName);
   }
}
