﻿using Factory.Core.MessagingAssistant.Entities;
using System.Threading.Tasks;

namespace Factory.Core.MessagingAssistant.Interfaces
{
   public interface ISmsApiService
   {
      public Task SendMessageAsync(Message message);
   }
}
