﻿namespace Factory.Core.MessagingAssistant.Interfaces
{
   public interface ITelegramBotApiService
   {
      string ProcessIncomingMessage(string message);
   }
}
