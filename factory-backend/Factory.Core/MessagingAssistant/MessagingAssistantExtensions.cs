﻿using Factory.Core.Interfaces.MessagingAssistant;
using Factory.Core.MessagingAssistant.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Factory.Core.MessagingAssistant.Services
{
   public static class MessagingAssistantExtensions
   {
      public static IServiceCollection AddMessagingAssistantCore(this IServiceCollection services)
      {
         services.AddTransient<ITelegramBotApiService, BotMessagesService>();
         services.AddTransient<IMessagingAssistantService, MessagingAssistantService>();
         return services;
      }
   }
}
