﻿using Factory.Core.Interfaces.MessagingAssistant;
using Factory.Core.MessagingAssistant.Dtos;
using Factory.Core.MessagingAssistant.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.MessagingAssistant.Services
{
   public class MessagingAssistantService : IMessagingAssistantService
   {
      private readonly IMessagingProducer messagingProducer;
      private readonly IMessageHistoryRepository messageHistoryRepository;

      public MessagingAssistantService(IMessagingProducer messagingService, IMessageHistoryRepository messageHistoryRepository)
      {
         this.messagingProducer = messagingService;
         this.messageHistoryRepository = messageHistoryRepository;
      }

      public async Task SendMessageAsync(MessageDto message)
      {
         this.messagingProducer.PublishMessage(message.Content, message.QueueName);
      }

      public List<string> GetMessageHistory()
      {
         return this.messageHistoryRepository.GetAll();
      }
   }
}
