﻿using Factory.Core.RunningEvents.Entities;

namespace Factory.Core.RunningEvents.Interfaces
{
   public static class Mapping
   {
      public static RunningEventDto ToDto(this IRunningEvent runningEvent)
      {
         return new RunningEventDto()
         {
            Id = runningEvent.Id,
            Description = runningEvent.Description,
            Name = runningEvent.Name,
            EventDate = runningEvent.EventDate
         };
      }

      public static IRunningEvent FromDto(this RunningEventDto runningEvent)
      {
         return new RunningEvent()
         {
            Id = runningEvent.Id,
            Description = runningEvent.Description,
            Name = runningEvent.Name,
            EventDate = runningEvent.EventDate
         };
      }
   }
}
