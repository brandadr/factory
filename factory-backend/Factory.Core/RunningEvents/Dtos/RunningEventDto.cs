﻿using System;

namespace Factory.Core.RunningEvents.Interfaces
{
   public class RunningEventDto : IRunningEvent
   {
      public int Id { get; set; }

      public string Name { get; set; }

      public string Description { get; set; }

      public DateTime EventDate { get; set; }
   }
}
