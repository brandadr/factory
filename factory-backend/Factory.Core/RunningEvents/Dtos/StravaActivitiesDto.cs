﻿using System.Collections.Generic;

namespace Factory.Core.RunningEvents.Dtos
{
    public class StravaActivitiesDto
    {
        public int NumberActivities => this.StravaActivities.Count;

        public List<StravaActivityDto> StravaActivities { get; set; }
    }
}
