﻿namespace Factory.Core.RunningEvents.Dtos
{
    public class StravaActivityDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
