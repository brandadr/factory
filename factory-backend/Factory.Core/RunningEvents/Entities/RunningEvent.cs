﻿using Factory.Core.RunningEvents.Interfaces;
using System;

namespace Factory.Core.RunningEvents.Entities
{
   internal class RunningEvent : IRunningEvent
   {
      public int Id { get; set; }

      public string Name { get; set; }

      public string Description { get; set; }

      public DateTime EventDate { get; set; }
   }
}