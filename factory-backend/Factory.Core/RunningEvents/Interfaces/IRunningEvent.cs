﻿using System;

namespace Factory.Core.RunningEvents.Interfaces
{
   public interface IRunningEvent
   {
      int Id { get; set; }

      string Name { get; set; }

      string Description { get; set; }

      DateTime EventDate { get; set; }
   }
}
