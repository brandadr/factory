﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.RunningEvents.Interfaces
{
   public interface IRunningEventRepository
   {
      Task<IReadOnlyList<IRunningEvent>> GetAllAsync();

      Task AddOrUpdate(IRunningEvent runningEvent);

      Task DeleteAsync(int eventId);

      Task<IRunningEvent> GetByIdAsync(int eventId);

      Task<int> SaveChangesAsync();
   }
}