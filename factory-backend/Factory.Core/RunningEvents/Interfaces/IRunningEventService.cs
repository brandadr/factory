﻿using Factory.Core.RunningEvents.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.RunningEvents.Interfaces
{
    public interface IRunningEventService
    {
        Task<IReadOnlyList<RunningEventDto>> GetRunningEventsAsync();

        Task<RunningEventDto> GetRunningEventAsync(int eventId);

        Task SaveRunningEventAsync(RunningEventDto runningEvent);

        Task DeleteRunningEventAsync(int eventId);

        Task GenerateStravaAccessTokens(string code);

        Task<StravaActivitiesDto> GetActivities();

        Task<bool> IsTokenExpired();
    }
}
