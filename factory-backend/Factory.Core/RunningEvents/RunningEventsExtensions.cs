﻿using Factory.Core.RunningEvents.Interfaces;
using Factory.Core.RunningEvents.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Factory.Core
{
   public static class RunningEventsExtensions
   {
      public static IServiceCollection AddRunningEventsCore(this IServiceCollection services)
      {
         services.AddTransient<IRunningEventService, RunningEventService>();
         return services;
      }
   }
}