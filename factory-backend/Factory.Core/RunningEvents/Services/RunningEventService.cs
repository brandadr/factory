﻿using Factory.Core.RunningEvents.Dtos;
using Factory.Core.RunningEvents.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Factory.Core.RunningEvents.Services
{
    internal class RunningEventService : IRunningEventService
    {
        private readonly IRunningEventRepository runningEventRepository;
        private readonly IOptions<StravaConfig> stravaConfig;
        private readonly IMemoryCache memoryCache;
        private StravaAuthorizationTokenResponse authorizationTokenResponse;

        public RunningEventService(IRunningEventRepository runningEventRepository, IOptions<StravaConfig> stravaConfig, IMemoryCache memoryCache)
        {
            this.runningEventRepository = runningEventRepository;
            this.stravaConfig = stravaConfig;
            this.memoryCache = memoryCache;
        }

        public async Task<IReadOnlyList<RunningEventDto>> GetRunningEventsAsync()
        {
            var events = await this.runningEventRepository.GetAllAsync();
            return events.Select(e => e.ToDto()).ToList();
        }

        public async Task<RunningEventDto> GetRunningEventAsync(int eventId)
        {
            var runningEvent = await this.runningEventRepository.GetByIdAsync(eventId);
            return runningEvent.ToDto();
        }

        public async Task SaveRunningEventAsync(RunningEventDto runningEvent)
        {
            await this.runningEventRepository.AddOrUpdate(runningEvent.FromDto());
            await this.runningEventRepository.SaveChangesAsync();
        }

        public async Task DeleteRunningEventAsync(int eventId)
        {
            await this.runningEventRepository.DeleteAsync(eventId);
            await this.runningEventRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Code will be provided by calling: https://www.strava.com/oauth/authorize?client_id=your_client_id&redirect_uri=http://localhost&response_type=code&scope=activity:read_all
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task GenerateStravaAccessTokens(string code)
        {
            try
            {
                var clientId = stravaConfig.Value.ClientId;
                var clientSecret = stravaConfig.Value.ClientSecret;
                var json = JsonConvert.SerializeObject(new StravaAuthorizationCodePostContent
                {
                    client_id = clientId,
                    client_secret = clientSecret,
                    code = code,
                    grant_type = "authorization_code"
                });
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                using var client = new HttpClient();
                var url = $"https://www.strava.com/oauth/token?client_id={clientId}&client_secret={clientSecret}&code={code}&grant_type=authorization_code";
                var response = await client.PostAsync(url, content);
                string responseBody = await response.Content.ReadAsStringAsync();
                var authorizationTokenResponse = JsonConvert.DeserializeObject<StravaAuthorizationTokenResponse>(responseBody);

                this.memoryCache.Set(nameof(authorizationTokenResponse.access_token), authorizationTokenResponse.access_token);
                this.memoryCache.Set(nameof(authorizationTokenResponse.refresh_token), authorizationTokenResponse.refresh_token);
                this.memoryCache.Set(nameof(authorizationTokenResponse.expires_at), authorizationTokenResponse.expires_at);
            }
            catch
            {
                //Todo log error
                throw;
            }

        }

        public async Task<StravaActivitiesDto> GetActivities()
        {
            List<StravaActivityDto> activities = new List<StravaActivityDto>();

            if (this.memoryCache.TryGetValue(nameof(authorizationTokenResponse.access_token), out var access_token))
            {
                using var client = new HttpClient();
                var activitiesUrl = $"https://www.strava.com/api/v3/athlete/activities?access_token={access_token}";
                var response = await client.GetAsync(activitiesUrl);
                string responseBody = await response.Content.ReadAsStringAsync();
                activities = JsonConvert.DeserializeObject<List<StravaActivityDto>>(responseBody);
            }

            return new StravaActivitiesDto() { StravaActivities = activities };
        }

        public Task<bool> IsTokenExpired()
        {
            if (this.memoryCache.TryGetValue(nameof(authorizationTokenResponse.expires_at), out var expires_at))
            {
                var expiresAtSeconds = (System.Int64)expires_at;
                var tokenExpiresAtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).AddSeconds(Convert.ToDouble(expiresAtSeconds)).ToLocalTime();
                var isExpired = DateTime.Now > tokenExpiresAtDateTime;
                return Task.FromResult(isExpired);
            }

            return Task.FromResult(true);
        }

        private class StravaAuthorizationCodePostContent
        {
            public string client_id { get; set; }
            public string client_secret { get; set; }
            public string code { get; set; }
            public string grant_type { get; set; }
        }

        private class StravaAuthorizationTokenResponse
        {
            public string refresh_token { get; set; }
            public string access_token { get; set; }
            public long expires_at { get; set; }
        }
    }
}
