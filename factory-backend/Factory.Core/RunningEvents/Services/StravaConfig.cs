﻿namespace Factory.Core.RunningEvents.Services
{
    public class StravaConfig
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
