﻿using Factory.Core.TimetableAssistant.Entities;

namespace Factory.Core.TimetableAssistant.Dtos
{
   public static class Mapping
   {
      public static TimetableConnectionDto ToDto(this ITimetableConnection timetableConnection)
      {
         return new TimetableConnectionDto()
         {
            Id = timetableConnection.Id,
            Name = timetableConnection.Name,
            UserId = timetableConnection.UserId,
            Departure = timetableConnection.Departure
         };
      }

      public static TimetableConnection FromDto(this TimetableConnectionDto timetableConnection)
      {
         return new TimetableConnection()
         {
            Id = timetableConnection.Id,
            Name = timetableConnection.Name,
            UserId = timetableConnection.UserId,
            Departure = timetableConnection.Departure
         };
      }
   }
}
