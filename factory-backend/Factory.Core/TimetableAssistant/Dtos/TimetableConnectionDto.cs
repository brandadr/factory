﻿namespace Factory.Core.TimetableAssistant.Dtos
{
   public class TimetableConnectionDto
   {
      public int Id { get; set; }

      public string UserId { get; set; }

      public string Name { get; set; }

      public int Departure { get; set; }
   }
}
