﻿namespace Factory.Core.TimetableAssistant.Entities
{
   public interface ITimetableConnection
   {
      int Id { get; set; }

      string UserId { get; set; }

      string Name { get; set; }

      int Departure { get; set; }
   }
}