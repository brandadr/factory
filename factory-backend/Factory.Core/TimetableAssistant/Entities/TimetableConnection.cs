﻿namespace Factory.Core.TimetableAssistant.Entities
{
   public class TimetableConnection : ITimetableConnection
   {
      public int Id { get; set; }

      public string UserId { get; set; }

      public string Name { get; set; }

      public int Departure { get; set; }
   }
}
