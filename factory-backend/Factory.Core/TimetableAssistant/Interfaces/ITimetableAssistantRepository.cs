﻿using Factory.Core.TimetableAssistant.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.TimetableAssistant.Interfaces
{
   public interface ITimetableAssistantRepository
   {
      Task<IReadOnlyList<ITimetableConnection>> GetAllAsync();

      Task AddOrUpdate(ITimetableConnection timetableConnection);

      Task DeleteAsync(int eventId);

      Task<ITimetableConnection> GetByIdAsync(int eventId);

      Task<int> SaveChangesAsync();
   }
}
