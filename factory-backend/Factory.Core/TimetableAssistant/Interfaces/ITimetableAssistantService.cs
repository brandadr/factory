﻿using Factory.Core.TimetableAssistant.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Factory.Core.TimetableAssistant.Interfaces
{
   public interface ITimetableAssistantService
   {
      Task<IReadOnlyList<TimetableConnectionDto>> GetTimetableConnectionsAsync();

      Task SaveTimetableConnectionAsync(TimetableConnectionDto timetableConnection);

      Task DeleteTimetableConnectionAsync(int id);

      Task<TimetableConnectionDto> GetTimetableConnectionAsync(int id);
   }
}
