﻿using Factory.Core.TimetableAssistant.Dtos;
using Factory.Core.TimetableAssistant.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Factory.Core.TimetableAssistant.Services
{
   internal class TimetableAssistantService : ITimetableAssistantService
   {
      private readonly ITimetableAssistantRepository timetableAssistantRepository;

      public TimetableAssistantService(ITimetableAssistantRepository timetableAssistantRepository)
      {
         this.timetableAssistantRepository = timetableAssistantRepository;
      }

      public async Task DeleteTimetableConnectionAsync(int id)
      {
         await this.timetableAssistantRepository.DeleteAsync(id);
         await this.timetableAssistantRepository.SaveChangesAsync();
      }

      public async Task<TimetableConnectionDto> GetTimetableConnectionAsync(int id)
      {
         var connection = await this.timetableAssistantRepository.GetByIdAsync(id);
         return connection.ToDto();
      }

      public async Task<IReadOnlyList<TimetableConnectionDto>> GetTimetableConnectionsAsync()
      {
         var connections = await this.timetableAssistantRepository.GetAllAsync();
         return connections.Select(e => e.ToDto()).ToList();
      }

      public async Task SaveTimetableConnectionAsync(TimetableConnectionDto timetableConnection)
      {
         await this.timetableAssistantRepository.AddOrUpdate(timetableConnection.FromDto());
         await this.timetableAssistantRepository.SaveChangesAsync();
      }
   }
}