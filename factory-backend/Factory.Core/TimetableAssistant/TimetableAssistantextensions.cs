﻿using Factory.Core.TimetableAssistant.Interfaces;
using Factory.Core.TimetableAssistant.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Factory.Core.TimetableAssistant
{
   public static class TimetableAssistantextensions
   {
      public static IServiceCollection AddTimeTableAssistantCore(this IServiceCollection services)
      {
         services.AddTransient<ITimetableAssistantService, TimetableAssistantService>();
         return services;
      }
   }
}
