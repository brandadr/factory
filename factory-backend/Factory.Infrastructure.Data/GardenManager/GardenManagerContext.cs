﻿using Microsoft.EntityFrameworkCore;

namespace Factory.Infrastructure.Data.GardenManager
{
   internal class GardenManagerContext : DbContext
   {
      public DbSet<GardenManagerLogDao> GardenManagerLogs { get; set; }

      public GardenManagerContext(DbContextOptions<GardenManagerContext> options) : base(options)
      {

      }
   }
}
