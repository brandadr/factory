﻿using Factory.Core.GardenManager.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Factory.Infrastructure.Data.GardenManager
{
   public static class GardenManagerExtensions
   {
      public static IServiceCollection AddGardenManagerData(this IServiceCollection services)
      {
         var provider = services.BuildServiceProvider();

         services.AddDbContext<GardenManagerContext>(options =>
            options.UseSqlServer(
               provider.GetRequiredService<IConfiguration>().GetConnectionString("Factory"))
         );

         services.AddTransient<IGardenManagerRepository, GardenManagerRepository>();

         return services;
      }

      public static void ConfigureGardenManagerData(this IServiceProvider provider)
      {
         using (var scope = provider.CreateScope())
         {
            var context = scope.ServiceProvider.GetService<GardenManagerContext>();
            if (context != null && context.Database != null)
            {
               context.Database.Migrate();
            }
         }
      }
   }
}
