﻿using Factory.Core.GardenManager.Interfaces;
using System;

namespace Factory.Infrastructure.Data.GardenManager
{
   internal class GardenManagerLogDao : IGardenManagerLog
   {
      public int Id { get; set; }

      public string Message { get; set; }

      public DateTime EventDate { get; set; }
   }
}
