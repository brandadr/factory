﻿using Factory.Core.GardenManager.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Factory.Infrastructure.Data.GardenManager
{
   internal class GardenManagerRepository : IGardenManagerRepository
   {
      private readonly GardenManagerContext context;

      public GardenManagerRepository(GardenManagerContext context)
      {
         this.context = context;
      }

      public async Task<IGardenManagerLog> GetByIdAsync(int logId)
      {

         return await Task.FromResult(this.context.GardenManagerLogs.FirstOrDefault(r => r.Id == logId));

      }

      public async Task<IReadOnlyList<IGardenManagerLog>> GetAllAsync()
      {

         return await Task.FromResult(this.context.GardenManagerLogs.ToList());

      }

      public async Task AddOrUpdate(IGardenManagerLog runningEvent)
      {

         var existingRunningEvent = this.context.GardenManagerLogs.FirstOrDefault(r => r.Id == runningEvent.Id);
         var data = new GardenManagerLogDao()
         {
            Message = runningEvent.Message,
            EventDate = runningEvent.EventDate
         };


         if (existingRunningEvent != null)
         {
            context.GardenManagerLogs.Update(data);
         }
         else
         {
            await context.GardenManagerLogs.AddAsync(data);
         }
      }

      public Task DeleteAsync(int logId)
      {
         var existingLog = context.GardenManagerLogs.FirstOrDefault(r => r.Id == logId);
         if (existingLog != null)
         {
            context.GardenManagerLogs.Remove(existingLog);
         }

         return Task.CompletedTask;
      }

      public async Task<int> SaveChangesAsync()
      {
         return await this.context.SaveChangesAsync();
      }
   }
}
