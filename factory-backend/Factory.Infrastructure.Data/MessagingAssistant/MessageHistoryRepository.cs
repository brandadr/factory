﻿using Factory.Core.MessagingAssistant.Interfaces;
using System.Collections.Generic;

namespace Factory.Infrastructure.Data.MessagingAssistant
{
   internal class MessageHistoryRepository : IMessageHistoryRepository
   {
      private List<string> history;

      public MessageHistoryRepository()
      {
         this.history = new List<string>();
      }

      public void Add(string message)
      {
         this.history.Add(message);
      }

      public List<string> GetAll()
      {
         return this.history;
      }
   }
}
