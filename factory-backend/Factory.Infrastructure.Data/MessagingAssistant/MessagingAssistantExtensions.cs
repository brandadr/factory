﻿using Factory.Core.MessagingAssistant.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Factory.Infrastructure.Data.MessagingAssistant
{
   public static class MessagingAssistantExtensions
   {
      public static IServiceCollection AddMessagingAssistantData(this IServiceCollection services)
      {
         services.AddSingleton<IMessageHistoryRepository, MessageHistoryRepository>();

         return services;
      }
   }
}
