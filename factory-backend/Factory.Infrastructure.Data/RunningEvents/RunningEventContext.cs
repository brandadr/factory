﻿using Factory.Infrastructure.Data.RunningEvents;
using Microsoft.EntityFrameworkCore;

namespace Factory.Infrastructure.Presistence.RunningEvents
{
   internal class RunningEventContext : DbContext
   {
      public DbSet<RunningEventData> RunningEvents { get; set; }

      public RunningEventContext(DbContextOptions<RunningEventContext> options) : base(options)
      {

      }
   }
}