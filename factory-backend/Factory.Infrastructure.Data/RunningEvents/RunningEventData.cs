﻿using Factory.Core.RunningEvents.Interfaces;
using System;

namespace Factory.Infrastructure.Data.RunningEvents
{
   internal class RunningEventData : IRunningEvent

   {
      public int Id { get; set; }

      public string Name { get; set; }

      public string Description { get; set; }

      public DateTime EventDate { get; set; }
   }
}
