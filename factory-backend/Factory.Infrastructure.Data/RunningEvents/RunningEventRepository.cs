﻿using Factory.Core.RunningEvents.Interfaces;
using Factory.Infrastructure.Data.RunningEvents;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Factory.Infrastructure.Presistence.RunningEvents
{
   internal class RunningEventRepository : IRunningEventRepository
   {
      private readonly RunningEventContext context;

      public RunningEventRepository(RunningEventContext context)
      {
         this.context = context;
      }

      public async Task<IRunningEvent> GetByIdAsync(int eventId)
      {

         return await Task.FromResult(this.context.RunningEvents.FirstOrDefault(r => r.Id == eventId));

      }

      public async Task<IReadOnlyList<IRunningEvent>> GetAllAsync()
      {

         return await Task.FromResult(this.context.RunningEvents.ToList());

      }

      public async Task AddOrUpdate(IRunningEvent runningEvent)
      {

         var existingRunningEvent = this.context.RunningEvents.FirstOrDefault(r => r.Id == runningEvent.Id);
         var data = new RunningEventData()
         {
            Name = runningEvent.Name,
            Description = runningEvent.Description,
            EventDate = runningEvent.EventDate
         };


         if (existingRunningEvent != null)
         {
            context.RunningEvents.Update(data);
         }
         else
         {
            await context.RunningEvents.AddAsync(data);
         }
      }

      public Task DeleteAsync(int eventId)
      {
         var existingRunningEvent = context.RunningEvents.FirstOrDefault(r => r.Id == eventId);
         if (existingRunningEvent != null)
         {
            context.RunningEvents.Remove(existingRunningEvent);
         }

         return Task.CompletedTask;
      }

      public async Task<int> SaveChangesAsync()
      {
         return await this.context.SaveChangesAsync();
      }
   }
}