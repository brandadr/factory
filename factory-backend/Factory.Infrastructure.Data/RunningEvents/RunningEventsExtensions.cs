﻿using Factory.Core.RunningEvents.Interfaces;
using Factory.Infrastructure.Presistence.RunningEvents;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Factory.Infrastructure.Data
{
   public static class RunningEventsExtensions
   {
      public static IServiceCollection AddRunningEventsData(this IServiceCollection services)
      {
         var provider = services.BuildServiceProvider();

         services.AddDbContext<RunningEventContext>(options => options.UseSqlServer(provider.GetRequiredService<IConfiguration>().GetConnectionString("Factory")));
         services.AddTransient<IRunningEventRepository, RunningEventRepository>();

         return services;
      }

      public static void ConfigureRunningEventsData(this IServiceProvider provider)
      {
         using (var scope = provider.CreateScope())
         {
            var context = scope.ServiceProvider.GetService<RunningEventContext>();
            if (context != null && context.Database != null)
            {
               context.Database.Migrate();
            }
         }
      }
   }
}
