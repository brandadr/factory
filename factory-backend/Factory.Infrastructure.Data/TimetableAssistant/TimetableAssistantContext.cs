﻿using Microsoft.EntityFrameworkCore;

namespace Factory.Infrastructure.Data.TimetableAssistant
{
   internal class TimetableAssistantContext : DbContext
   {
      public DbSet<TimetableConnectionData> TimetableConnections { get; set; }

      public TimetableAssistantContext(DbContextOptions<TimetableAssistantContext> options) : base(options)
      {

      }
   }
}
