﻿using Factory.Core.TimetableAssistant.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Factory.Infrastructure.Data.TimetableAssistant
{
   public static class TimetableAssistantExtensions
   {
      public static IServiceCollection AddTimetableAssistantData(this IServiceCollection services)
      {
         var provider = services.BuildServiceProvider();

         services.AddDbContext<TimetableAssistantContext>(options => options.UseSqlServer(provider.GetRequiredService<IConfiguration>().GetConnectionString("Factory")));
         services.AddTransient<ITimetableAssistantRepository, TimetableAssistantRepository>();

         return services;
      }

      public static void ConfigureTimetableAssistantData(this IServiceProvider provider)
      {
         using (var scope = provider.CreateScope())
         {
            var context = scope.ServiceProvider.GetService<TimetableAssistantContext>();
            if (context != null && context.Database != null)
            {
               context.Database.Migrate();
            }
         }
      }
   }
}
