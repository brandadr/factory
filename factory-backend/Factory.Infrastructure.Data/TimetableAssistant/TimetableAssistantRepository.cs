﻿using Factory.Core.TimetableAssistant.Entities;
using Factory.Core.TimetableAssistant.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Factory.Infrastructure.Data.TimetableAssistant
{
   internal class TimetableAssistantRepository : ITimetableAssistantRepository
   {
      private readonly TimetableAssistantContext context;

      public TimetableAssistantRepository(TimetableAssistantContext context)
      {
         this.context = context;
      }

      public async Task<ITimetableConnection> GetByIdAsync(int id)
      {
         return await Task.FromResult(this.context.TimetableConnections.FirstOrDefault(r => r.Id == id));
      }

      public async Task<IReadOnlyList<ITimetableConnection>> GetAllAsync()
      {
         return await Task.FromResult(this.context.TimetableConnections.ToList());
      }

      public async Task AddOrUpdate(ITimetableConnection timetableConnection)
      {
         var existingTimetableConnection = this.context.TimetableConnections.FirstOrDefault(r => r.Id == timetableConnection.Id);

         if (existingTimetableConnection != null)
         {
            existingTimetableConnection.Name = timetableConnection.Name;
            existingTimetableConnection.Departure = timetableConnection.Departure;
         }
         else
         {
            await this.context.TimetableConnections.AddAsync(new TimetableConnectionData()
            {
               Name = timetableConnection.Name,
               Departure = timetableConnection.Departure,
               UserId = timetableConnection.UserId
            });


         }
      }

      public Task DeleteAsync(int id)
      {

         var existingTimetableConnection = this.context.TimetableConnections.FirstOrDefault(r => r.Id == id);
         if (existingTimetableConnection != null)
         {
            this.context.TimetableConnections.Remove(existingTimetableConnection);
         }

         return Task.CompletedTask;
      }

      public async Task<int> SaveChangesAsync()
      {
         return await this.context.SaveChangesAsync();
      }
   }
}
