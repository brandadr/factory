﻿using Factory.Core.TimetableAssistant.Entities;

namespace Factory.Infrastructure.Data.TimetableAssistant
{
   internal class TimetableConnectionData : ITimetableConnection
   {
      public int Id { get; set; }

      public string UserId { get; set; }

      public string Name { get; set; }

      public int Departure { get; set; }
   }
}
