﻿using Factory.Core.GardenManager.Interfaces;
using Factory.Infrastructure.Http.Clients.RaspberryPi;
using Factory.Infrastructure.Http.Clients.Shelly;

namespace Factory.Infrastructure.Http.Clients
{
   internal class DeviceClientFactory : IDeviceClientFactory
   {
      public IRaspberryDeviceClient CreateRaspberryPiClient(string ipAddress, string username = null, string password = null)
      {
         return new RaspberryPiClient(ipAddress, username, password);
      }

      public IShellyDeviceClient CreateShellyClient(string ipAddress, string username = null, string password = null)
      {
         return new ShellyClient(ipAddress, username, password);
      }
   }
}
