﻿using Factory.Core.GardenManager.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Factory.Infrastructure.Http.Clients
{
   public static class HttpClientsExtensions
   {
      public static IServiceCollection AddHttpClientsApi(this IServiceCollection services)
      {
         services.AddTransient<IDeviceClientFactory, DeviceClientFactory>();

         return services;
      }
   }
}
