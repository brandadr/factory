﻿using Factory.Core.GardenManager.Interfaces;
using Renci.SshNet;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Factory.Infrastructure.Http.Clients.RaspberryPi
{
   internal class RaspberryPiClient : IRaspberryDeviceClient
   {
      private readonly string ipAddress;
      private readonly string username;
      private readonly string password;

      public string IpAddress => this.ipAddress;

      public RaspberryPiClient(string ipAddress, string username, string password)
      {
         this.ipAddress = ipAddress;
         this.username = username;
         this.password = password;
      }

      public async Task<bool> GetGpioStateAsync(int gpioId)
      {
         var result = this.ExecuteCommand(@$"cat /sys/class/gpio/gpio{gpioId}/value");

         return await Task.FromResult(result == "1");
      }

      public async Task SetGpio(int gpioId, bool isOn)
      {
         var state = isOn ? "1" : "0";
         this.ExecuteCommand(@$"echo {state} > /sys/class/gpio/gpio{gpioId}/value");
      }

      public async Task<bool> IsOnlineAsync()
      {
         var isConnected = false;
         try
         {
            using (var client = new SshClient(ipAddress, 22, username, password))
            {
               client.Connect();
               var result = client.RunCommand(@"ls .\.");
               isConnected = client.IsConnected;
            }
         }
         catch
         {
            //Do nothing...
         }

         return await Task.FromResult(isConnected);
      }

      public void ActivateGpio(int gpioId)
      {
         this.ExecuteCommand(@$"echo {gpioId} > /sys/class/gpio/export");
      }

      public void DeactivateGpio(int gpioId)
      {
         this.ExecuteCommand(@$"echo {gpioId} > /sys/class/gpio/unexport");
      }

      public void SetGpioAsOutput(int gpioId)
      {
         this.ExecuteCommand(@$"echo out > /sys/class/gpio/gpio{gpioId}/direction");
      }

      public void SetGpioAsInput(int gpioId)
      {
         this.ExecuteCommand(@$"echo in > /sys/class/gpio/gpio{gpioId}/direction");
      }

      public string ExecuteCommand(string command)
      {
         var resultString = "";

         try
         {
            using (var sshClient = new SshClient(ipAddress, 22, username, password))
            {

               sshClient.Connect();

               var cmd = sshClient.CreateCommand(command);
               var result = cmd.BeginExecute();

               using (var reader = new StreamReader(cmd.OutputStream))
               {
                  while (!reader.EndOfStream || !result.IsCompleted)
                  {
                     string line = reader.ReadLine();

                     if (line != null)
                     {
                        resultString += line;
                     }
                  }
                  sshClient.Disconnect();
               }
            }
         }
         catch (Exception error)
         {
            resultString = error.Message;
            //Do nothing...
         }

         return resultString;
      }
   }
}
