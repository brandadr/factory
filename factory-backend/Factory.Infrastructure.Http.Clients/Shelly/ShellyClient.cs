﻿using Factory.Core.GardenManager.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;

namespace Factory.Infrastructure.Http.Clients.Shelly
{
   internal class ShellyClient : IShellyDeviceClient
   {
      private readonly string ipAddress;
      private readonly string username;
      private readonly string password;

      public ShellyClient(string ipAddress, string username, string password)
      {
         this.ipAddress = ipAddress;
         this.username = username;
         this.password = password;
      }

      public string IpAddress => this.ipAddress;

      public async Task<bool> GetGpioStateAsync(int relayId)
      {
         try
         {
            var client = new HttpClient();
            var url = $"http://{ipAddress}/relay/{relayId}";
            var respone = await client.GetStringAsync(url);
            return respone.Contains("ison\":true");
         }
         catch
         {
            //Todo: Loggen und Fehlerhandling
            return false;
         }
      }

      public async Task<bool> IsOnlineAsync()
      {
         var isConnected = false;

         try
         {
            var client = new HttpClient();
            var url = $"http://{ipAddress}/relay/0";
            var respone = await client.GetStringAsync(url);
            isConnected = true;
         }
         catch
         {
            //Do noting...
         }

         return isConnected;
      }

      public async Task SetGpio(int relayId, bool isRelayOn)
      {
         try
         {
            var client = new HttpClient();
            var state = isRelayOn ? "on" : "off";
            var url = $"http://{ipAddress}/relay/{relayId}?turn={state}";
            await client.GetAsync(url);
         }
         catch
         {
            //Todo: Loggen und Fehlerhandling
         }
      }
   }
}
