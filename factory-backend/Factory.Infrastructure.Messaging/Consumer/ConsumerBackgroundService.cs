﻿using Factory.Core.MessagingAssistant.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Factory.Infrastructure.Messaging.Consumer
{
   internal class ConsumerBackgroundService : BackgroundService
   {
      private readonly ILogger logger;
      private readonly string connectionString;
      private IConnection connection;
      private IModel channel;
      private readonly string queueName = "factory-queue";
      private IMessageHistoryRepository messageHistoryRepository;

      public ConsumerBackgroundService(IOptions<RabbitMQConfig> rabbitMQConfigOptions, ILoggerFactory loggerFactory, IMessageHistoryRepository messageHistoryRepository)
      {
         this.logger = loggerFactory.CreateLogger<ConsumerBackgroundService>();
         this.connectionString = rabbitMQConfigOptions.Value.Connection;
         this.Init();
         this.messageHistoryRepository = messageHistoryRepository;
      }

      protected override Task ExecuteAsync(CancellationToken stoppingToken)
      {
         try
         {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += Consumer_Received;

            channel.BasicConsume(queueName, true, consumer);
         }
         catch (Exception ex)
         {
            this.logger.LogInformation($"Error while registering rabbitMQ background service!", ex.Message);
         }

         return Task.CompletedTask;
      }

      private void Init()
      {
         try
         {
            var factory = new ConnectionFactory
            {
               Uri = new System.Uri(this.connectionString)
            };

            connection = factory.CreateConnection();

            channel = connection.CreateModel();

            channel.QueueDeclare(queueName,
               durable: true,
               exclusive: false,
               autoDelete: false,
               arguments: null);
         }
         catch (Exception ex)
         {
            this.logger.LogInformation($"Error while initializing rabbitMQ background service!", ex.Message);
         }
      }

      private void Consumer_Received(object sender, BasicDeliverEventArgs e)
      {
         var body = e.Body.ToArray();
         var message = Encoding.UTF8.GetString(body);
         this.logger.LogInformation($"Consumer Received; {message}");
         this.messageHistoryRepository.Add(message);
      }
   }
}
