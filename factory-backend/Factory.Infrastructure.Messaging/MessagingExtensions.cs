﻿using Factory.Core.Interfaces.MessagingAssistant;
using Factory.Infrastructure.Messaging.Consumer;
using Factory.Infrastructure.Messaging.Producer;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Factory.Infrastructure.Messaging
{
   public static class MessagingExtensions
   {
      public static IServiceCollection AddMessaging(this IServiceCollection services)
      {
         services.AddTransient<IMessagingProducer, MessagingProducer>();
         services.AddHostedService<ConsumerBackgroundService>();

         return services;
      }

      public static void ConfigureMessaging(this IServiceProvider provider)
      {

      }
   }
}
