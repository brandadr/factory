﻿
using Factory.Core.Interfaces.MessagingAssistant;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace Factory.Infrastructure.Messaging.Producer
{
   public class MessagingProducer : IMessagingProducer
   {
      private readonly string connectionString;

      public MessagingProducer(IOptions<RabbitMQConfig> rabbitMQConfigOptions)
      {
         this.connectionString = rabbitMQConfigOptions.Value.Connection;
      }

      public void PublishMessage(string message, string queueName = "factory-queue")
      {
         var producerName = "FactoryProducer";

         var factory = new ConnectionFactory
         {
            Uri = new System.Uri(this.connectionString)
         };

         using var connection = factory.CreateConnection();
         using var channel = connection.CreateModel();
         channel.QueueDeclare(queueName,
            durable: true,
            exclusive: false,
            autoDelete: false,
            arguments: null);
         var messageObject = new { Name = producerName, Message = message };
         var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messageObject));

         channel.BasicPublish("", queueName, null, body);
      }
   }
}
