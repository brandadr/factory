﻿namespace Factory.Infrastructure.Messaging
{
   public class RabbitMQConfig
   {
      public string Connection { get; set; }
   }
}
