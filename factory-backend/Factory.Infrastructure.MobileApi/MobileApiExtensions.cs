﻿using Factory.Core.MessagingAssistant.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Factory.Infrastructure.MobileApi
{
   public static class MobileApiExtensions
   {
      public static IServiceCollection AddMobileInfrastructureApi(this IServiceCollection services)
      {
         services.AddTransient<ISmsApiService, SmsService>();
         services.AddTransient<IBotService>(s => new TelegramBot(
            s.GetRequiredService<ITelegramBotApiService>(),
            s.GetRequiredService<IConfiguration>().GetSection("TelegramToken").Value));

         return services;
      }

      public static void ConfigureMobileInfrastructureApi(this IServiceProvider provider)
      {
         provider.GetRequiredService<IBotService>().StartReceiving();
      }
   }
}
