﻿using Factory.Core.MessagingAssistant.Entities;
using Factory.Core.MessagingAssistant.Interfaces;
using System.Threading.Tasks;

namespace Factory.Infrastructure.MobileApi
{
   public class SmsService : ISmsApiService
   {
      public Task SendMessageAsync(Message message)
      {
         return Task.CompletedTask;
      }
   }
}
