﻿using Factory.Core.MessagingAssistant.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace Factory.Infrastructure.MobileApi
{
   public class TelegramBot : IBotService
   {
      private ITelegramBotClient botClient;
      private ITelegramBotApiService telegramBotService;
      private readonly string token;

      public TelegramBot(ITelegramBotApiService telegramBotService, string token)
      {
         this.telegramBotService = telegramBotService;
         this.token = token;
      }

      public void StartReceiving()
      {
         botClient = new TelegramBotClient(this.token);
         botClient.OnMessage += this.OnMessage;
         botClient.StartReceiving();
      }

      public void StopReceiving()
      {
         botClient.OnMessage -= this.OnMessage;
         botClient.StopReceiving();
      }

      private async void OnMessage(object sender, MessageEventArgs e)
      {
         if (e.Message.Text != null)
         {
            var answer = this.telegramBotService.ProcessIncomingMessage(e.Message.Text);
            await botClient.SendTextMessageAsync(
              chatId: e.Message.Chat,
              text: answer
            );
         }
      }
   }
}
