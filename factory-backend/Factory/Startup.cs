using Factory.Api.MessagingAssistant;
using Factory.Api.TimetableAssistant;
using Factory.Core;
using Factory.Core.GardenManager;
using Factory.Core.GardenManager.Entities;
using Factory.Core.MessagingAssistant.Services;
using Factory.Core.RunningEvents.Services;
using Factory.Core.TimetableAssistant;
using Factory.Infrastructure.Data;
using Factory.Infrastructure.Data.GardenManager;
using Factory.Infrastructure.Data.MessagingAssistant;
using Factory.Infrastructure.Data.TimetableAssistant;
using Factory.Infrastructure.Http.Clients;
using Factory.Infrastructure.Messaging;
using factory_backend;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Security.Claims;

namespace Factory.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers().AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "factory_backend", Version = "v1" });
            });
            services.AddOptions();

            //Todo Options in GardenManagerExtensions
            services.Configure<ShellyConfig>(Configuration.GetSection(nameof(ShellyConfig)));
            services.Configure<RaspberryPiConfig>(Configuration.GetSection(nameof(RaspberryPiConfig)));
            services.Configure<RabbitMQConfig>(Configuration.GetSection(nameof(RabbitMQConfig)));
            services.Configure<StravaConfig>(Configuration.GetSection(nameof(StravaConfig)));
            services.AddGardenManagerData();
            services.AddHttpClientsApi();
            services.AddGardenManagerCore();

            services.AddRunningEventsData();
            services.AddRunningEventsCore();
            services.AddRunningEventsApi();

            services.AddTimetableAssistantData();
            services.AddTimeTableAssistantCore();
            services.AddTimetableAssistantApi();

            services.AddMessagingAssistantData();
            services.AddMessagingAssistantCore();
            services.AddMessagingAssistantApi();
            services.AddMessaging();

            services.AddMemoryCache();

            string domain = "https://dev-oo4ooobt.eu.auth0.com/";
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = domain;
                    options.Audience = "factory.adrianbrand.ch";
                    // If the access token does not have a `sub` claim, `User.Identity.Name` will be `null`. Map it to a different claim by setting the NameClaimType below.
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = ClaimTypes.NameIdentifier
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("read:running-events", policy => policy.Requirements.Add(new HasScopeRequirement("read:running-events", "https://dev-oo4ooobt.eu.auth0.com/")));
            });

            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "factory_backend v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x
               .AllowAnyMethod()
               .AllowAnyHeader()
               .AllowAnyOrigin()
               .SetIsOriginAllowed(origin => true));

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.ApplicationServices.ConfigureGardenManagerData();
            app.ApplicationServices.ConfigureRunningEventsData();
            app.ApplicationServices.ConfigureTimetableAssistantData();
            app.ApplicationServices.ConfigureMessaging();
        }
    }
}
