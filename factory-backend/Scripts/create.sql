CREATE TABLE TimetableConnections (
    Id int IDENTITY(1,1) PRIMARY KEY,
    UserId varchar(255) NOT NULL,
    Name varchar(255),
    Departure int
);

CREATE TABLE RunningEvents (
    Id int IDENTITY(1,1) PRIMARY KEY,
    Name varchar(255),
    Description varchar(255),
    EventDate date
);