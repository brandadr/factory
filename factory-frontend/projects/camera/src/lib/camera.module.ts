import { NgModule } from '@angular/core';
import { CameraComponent } from './components/camera/camera.component';
import { WebcamModule } from 'ngx-webcam';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [CameraComponent],
  imports: [
    WebcamModule,
    MatButtonModule
  ],
  exports: [
    CameraComponent
  ]
})
export class CameraModule { }
