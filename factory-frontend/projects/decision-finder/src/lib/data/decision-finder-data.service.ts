import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { DecisionItem, Option } from "./decision.interface";

@Injectable({
    providedIn: 'root'
})
export class DecisionFinderDataService {
    public decisionMock: Array<DecisionItem> = [
        {
            criteria: "Preis",
            options: [
                {
                    description: "Auto",
                    points: 1
                },
                {
                    description: "Velo",
                    points: 4
                }
            ]
        },
        {
            criteria: "Geschwindigkeit",
            options: [
                {
                    description: "Auto",
                    points: 3
                },
                {
                    description: "Velo",
                    points: 1
                }
            ]
        }
    ];

    public optionsMock: Array<Option> = [
        {
            description: "Auto",
            points: 0
        },
        {
            description: "Velo",
            points: 0
        }
    ];

    public getOptions(): Observable<Array<Option>> {
        return of(this.optionsMock);
    }

    public addOption(option: Option): Observable<any> {
        this.optionsMock.push(option);
        return of({});
    }

    public getDecisionItems(): Observable<Array<DecisionItem>> {
        return of(this.decisionMock);
    }

    public addDecisionItem(decisionItem: DecisionItem): Observable<any> {
        this.decisionMock.push(decisionItem);
        return of({});
    }

    public deleteDecisionItem(criteria: string): Observable<any> {
        this.decisionMock = this.decisionMock.filter(item => item.criteria !== criteria);
        return of({});
    }
}