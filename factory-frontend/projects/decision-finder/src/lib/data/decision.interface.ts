export interface Decision {
    decision: string;
    points: number;
}

export interface Option {
    description: string;
    points: number;
}

export interface DecisionItem {
    criteria: string;
    options: Array<Option>;
}