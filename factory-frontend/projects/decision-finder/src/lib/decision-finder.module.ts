import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { FactoryUiLibModule } from 'factory-ui';
import { DecisionFinderComponent } from './view/decision-finder/decision-finder.component';



@NgModule({
  declarations: [
    DecisionFinderComponent
  ],
  imports: [
    FactoryUiLibModule,
    MatButtonModule
  ],
  exports: [
    DecisionFinderComponent
  ]
})
export class DecisionFinderModule { }
