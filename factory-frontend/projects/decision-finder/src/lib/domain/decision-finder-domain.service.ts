import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { DecisionFinderDataService } from '../data/decision-finder-data.service';
import { Decision, DecisionItem, Option } from '../data/decision.interface';

@Injectable({
  providedIn: 'root'
})
export class DecisionFinderDomainService {
  private decisions: BehaviorSubject<Array<DecisionItem>> = new BehaviorSubject<Array<DecisionItem>>([]);
  public decisions$: Observable<Array<DecisionItem>> = this.decisions.asObservable();

  private options: BehaviorSubject<Array<Option>> = new BehaviorSubject<Array<Option>>([]);
  public options$: Observable<Array<Option>> = this.options.asObservable();

  constructor(private readonly decisionFinderDataService: DecisionFinderDataService) { }

  public init(): void {
    this.decisions$ = this.decisionFinderDataService.getDecisionItems();
    this.options$ = this.decisionFinderDataService.getOptions();
  }

  public setOption(option: Option): void {
    this.decisionFinderDataService.addOption(option).subscribe(() => this.init());
  }

  public setDecisionItem(decisionItem: DecisionItem): void {
    this.decisionFinderDataService.addDecisionItem(decisionItem).subscribe(() => this.init());
  }

  public clearDecisionItems(): void {
    this.decisions$.pipe(
      tap((decisions: Array<DecisionItem>) => {
        decisions.forEach((decision: DecisionItem) => {
          this.decisionFinderDataService.deleteDecisionItem(decision.criteria).subscribe(() => this.init());
        })
      })).subscribe();
  }

  public getDecision(tableData: Array<any>) : Decision {
    var results: Array<Option> = [];

    for(let item of tableData) {
      for (let itemInternal in item) {
        if (itemInternal !== "criteria") {
          results.push({description: itemInternal, points: item[itemInternal]})
        }
      }
    }

    var sumResults: Array<Option> = [];
    for (let result of results) {
      let sum = sumResults.find(s => s.description === result.description);
      if(sum) {
        sum.points += Number(result.points); //Todo: Why is this needed?
      } else {
        sumResults.push({description: result.description, points: result.points})
      }
    }

    let decision: Option = {description: 'undefined', points: -1};
    sumResults.forEach(result => {
      if (result.points > decision.points) {
        decision = result;
      }
    });

    return { points: decision.points, decision: decision.description };
  }
}
