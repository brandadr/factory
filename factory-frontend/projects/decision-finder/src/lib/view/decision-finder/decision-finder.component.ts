import { Component, OnInit } from '@angular/core';
import { MatOptionSelectionChange } from '@angular/material/core';
import { GenericDialogField, GenericDialogService, TableColumnDefinition, TableColumnType } from 'factory-ui';
import { tap } from 'rxjs/operators';
import { Decision, DecisionItem, Option } from '../../data/decision.interface';
import { DecisionFinderDomainService } from '../../domain/decision-finder-domain.service';

@Component({
  selector: 'lib-decision-finder',
  templateUrl: './decision-finder.component.html',
  styleUrls: ['./decision-finder.component.css']
})
export class DecisionFinderComponent implements OnInit {

  public dialogValues: Array<GenericDialogField> = [];
  public columnDefinitions: Array<TableColumnDefinition> = [];
  public tableData: Array<any> = [];

  constructor(
    public readonly genericDialogService: GenericDialogService,
    private readonly decisionFinderDomainService: DecisionFinderDomainService,
  ) { }

  public ngOnInit(): void {
    this.decisionFinderDomainService.init();
    this.initColumns();
    this.initData();
    this.initDialogValues();
  }

  public initDialogValues() : void {
    this.dialogValues = [];
    this.dialogValues.push({id: 'criteria', labelName: 'Kriterium', value: null});

    this.decisionFinderDomainService.options$.pipe(tap((options: Array<Option>)  => {
      options.forEach(option => {
        this.dialogValues.push({id: option.description, labelName: option.description, value: null});
      });
    })).subscribe();
  }

  public initData() : void {
    this.tableData = [];
    this.decisionFinderDomainService.decisions$.pipe(tap((items: Array<DecisionItem>)  => {
      items.forEach(item => {
        
        var object: {[k: string]: any} = {};
        object.criteria = item.criteria;

        item.options.forEach(option => {
          object[option.description] = option.points;
        })

        this.tableData.push(object);
      });

      this.tableData = [...this.tableData];
    })).subscribe();
  }

  public initColumns() : void {
    this.columnDefinitions= [];
    this.decisionFinderDomainService.options$.pipe(tap((options: Array<Option>)  => {
      this.columnDefinitions.push({id: 'criteria', description: 'Kriterium', type: TableColumnType.Text});
      options.forEach(option => {
        this.columnDefinitions.push({id: option.description, description: option.description, type: TableColumnType.Text});
      });
    })).subscribe();
  }

  public onAdd(): void {
    this.genericDialogService.openDialog(     
      "Wert hinzufügen",
      "Ergänze die Tabelle mit einem gewichteten Kriterium",
      this.dialogValues,
      () => {
        const criteria = this.dialogValues.find(d => d.id === 'criteria')?.value;
        const optionAuto = this.dialogValues.find(d => d.id === 'Auto')?.value;
        const optionVelo = this.dialogValues.find(d => d.id === 'Velo')?.value;

        this.decisionFinderDomainService.setDecisionItem({criteria, options: [{description: 'Auto', points: optionAuto}, {description: 'Velo', points: optionVelo}]});
        this.initData();
        this.initDialogValues();
        this.dialogValues.forEach(value => {
          value.value = null;
        });
      });
  } 

  public onAnalyze(): void {
    const decision: Decision = this.decisionFinderDomainService.getDecision(this.tableData);

    this.genericDialogService.openDialog(     
      "Entscheidung: " + decision.decision,
      `Punkte ${decision.points} 
      `,
      [],
      () => { },
      400);
  } 

  public onClearAll(): void {
    this.decisionFinderDomainService.clearDecisionItems();
    this.initData();
    this.initDialogValues();
  }

  public onAddCriteria(): void {
    this.columnDefinitions.push({id: 'test', description: 'Name', type: TableColumnType.Text});
    this.columnDefinitions = [...this.columnDefinitions];
  }
}
