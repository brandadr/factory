import { NgModule } from '@angular/core';
import { MessengerComponent } from './messenger.component';



@NgModule({
  declarations: [
    MessengerComponent
  ],
  imports: [
  ],
  exports: [
    MessengerComponent
  ]
})
export class MessengerModule { }
