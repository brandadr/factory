import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningEventsPageComponent } from './running-events-page.component';

describe('RunningEventsPageComponent', () => {
  let component: RunningEventsPageComponent;
  let fixture: ComponentFixture<RunningEventsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RunningEventsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningEventsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
