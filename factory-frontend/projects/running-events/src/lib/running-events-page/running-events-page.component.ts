import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { RunningEventsService } from '../running-events.service';

@Component({
  selector: 'lib-running-events-page',
  templateUrl: './running-events-page.component.html',
  styleUrls: ['./running-events-page.component.css']
})
export class RunningEventsPageComponent implements OnInit {
  public isTokenExpired$: Observable<boolean> = of(true);
  public activities$: Observable<any> = of(undefined);

  constructor(private route: ActivatedRoute, private runningEventsService: RunningEventsService) { 

  }

  public ngOnInit(): void {
    this.isTokenExpired$ = this.runningEventsService.getIsTokenExpired();

    const code = this.route.snapshot.queryParams['code'];
    if (!!code){
      this.runningEventsService.generateStravaAccessTokens(code).subscribe(() => {
        this.isTokenExpired$ = this.runningEventsService.getIsTokenExpired();
      });
    }

  }

  public onAuth() {
    window.open(environment.stravaAuthUrl, "_blank");
  }

  public onGetActivities() {
    this.activities$ = this.runningEventsService.getStravaActivities();
  }
}
