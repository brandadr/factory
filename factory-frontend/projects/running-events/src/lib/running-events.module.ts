import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { RunningEventsPageComponent } from './running-events-page/running-events-page.component';



@NgModule({
  declarations: [
    RunningEventsPageComponent
  ],
  imports: [
    FlexLayoutModule,
    CommonModule,
    MatButtonModule
  ],
  exports: [
    RunningEventsPageComponent
  ]
})
export class RunningEventsModule { }
