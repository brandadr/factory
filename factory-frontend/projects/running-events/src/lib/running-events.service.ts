import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RunningEventsService {

  private readonly stravaUrl: string = `${environment.backendUrl}/api/runningevent/strava`;

  constructor(private http: HttpClient) { }

  public generateStravaAccessTokens(code: string): Observable<any> {

      return this.http.get<any>(`${this.stravaUrl}/access-tokens/${code}`);
  }

  public getStravaActivities(): Observable<any> {

    return this.http.get<any>(`${this.stravaUrl}/activities`);
  }

  public getIsTokenExpired(): Observable<boolean> {

    return this.http.get<any>(`${this.stravaUrl}/is-token-expired`);
  }
}
