/*
 * Public API Surface of running-events
 */

export * from './lib/running-events.service';
export * from './lib/running-events-page/running-events-page.component';
export * from './lib/running-events.module';
