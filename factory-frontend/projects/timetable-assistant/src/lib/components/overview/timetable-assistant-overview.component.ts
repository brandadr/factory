import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TimetableDomainService } from '../../services/timetable.domain-service';
import { GenericDialogService } from 'factory-ui';
import { GenericDialogField } from 'factory-ui';

@Component({
  selector: 'lib-timetable-assistant-overview',
  templateUrl: './timetable-assistant-overview.component.html',
  styleUrls: ['./timetable-assistant-overview.component.css']
})
export class TimetableAssistantOverviewComponent implements OnInit {

  public time: string = "";
  public destination: string = "";

  public dialogValues: Array<GenericDialogField> = [
    {id:'to', labelName: 'Nach', value: ""},
    {id:'minute', labelName: 'Minute', value: ""},
  ];

  constructor(
    public readonly genericDialogService: GenericDialogService,
    public timetableDomainService: TimetableDomainService,
    public dialog: MatDialog) {
  }

  public ngOnInit(): void {
    this.timetableDomainService.init();
  }

  public openDialog(): void {

    this.genericDialogService.openDialog(     
      "Verbindung hinzufügen",
      "",
      this.dialogValues,
      () => {
        const to = this.dialogValues.find(f => f.id === 'to')?.value ?? '';
        const minute = this.dialogValues.find(f => f.id === 'minute')?.value ?? '';
        this.timetableDomainService.addConnection(to, minute);
      });
  }
}
