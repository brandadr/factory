export interface DialogData {
    time: number;
    destination: string;
  }