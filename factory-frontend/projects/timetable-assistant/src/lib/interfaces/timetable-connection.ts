export interface TimetableConnection {
    Id: number;
    
    UserId: string;

    Name: string;

    Departure: number;  
}

export interface TimetableConnectionDisplay {
    Name: string;

    Departure: Date | null;  
}