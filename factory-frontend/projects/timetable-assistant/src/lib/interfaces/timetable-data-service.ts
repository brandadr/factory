import { Observable } from "rxjs";
import { TimetableConnection } from "./timetable-connection";

export interface ITimetableDataService {
    getTimetableConnections(): Observable<Array<TimetableConnection>>;

    deleteTimeTableConnection(id: number): Observable<any>;

    postTimeTableConnection(destination: string, time: number): Observable<any>;
}