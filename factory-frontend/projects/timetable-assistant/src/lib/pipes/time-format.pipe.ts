import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'secondsToMinutesSeconds'
})
export class SecondsToMinutesSecondsPipe implements PipeTransform {
    transform(totalSeconds: number | null | undefined): string {

        if (!totalSeconds) {
            return '00:00'
        }
        const seconds = totalSeconds % 60;
        const minutes = (totalSeconds - seconds) / 60;
        const secondsText = seconds === 1 ? 'Sekunde' : 'Sekunden';
        const minutesText = minutes === 1 ? 'Minute' : 'Minuten';


        
        return `${minutes} ${minutesText} ${seconds} ${secondsText}`;
    }
}
