import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { TimetableConnection } from "../interfaces/timetable-connection";
import { ITimetableDataService } from "../interfaces/timetable-data-service";

@Injectable({
    providedIn: 'root'
})
export class TimetableMockDataService implements ITimetableDataService {

    public timeTableMock: Array<TimetableConnection> = [
        {
            Id: 0,
            UserId: "",
            Name: "Rom",
            Departure: 12,
        },
        {
            Id: 1,
            UserId: "",
            Name: "Paris",
            Departure: 46,
        }
    ];

    constructor() { }

    public getTimetableConnections(): Observable<Array<TimetableConnection>> {
        return of(this.timeTableMock);
    }

    public deleteTimeTableConnection(id: number): Observable<any> {
        this.timeTableMock = this.timeTableMock.filter(item => item.Id !== id);
        return of({});
    }

    public postTimeTableConnection(destination: string, time: number): Observable<any> {
        this.timeTableMock.push(
            {
                "Id": 0,
                "UserId": "admin",
                "Name": destination,
                "Departure": time
            }
        );

        return of({});
    }
}