import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { TimetableConnection } from "../interfaces/timetable-connection";
import { ITimetableDataService } from "../interfaces/timetable-data-service";

@Injectable({
    providedIn: 'root'
})
export class TimetableDataService implements ITimetableDataService {
    private readonly url: string = `${environment.backendUrl}/api/timetableassistant`;
    public timeTableMock: Array<TimetableConnection> = [
        {
            Id: 0,
            UserId: "",
            Name: "Rom",
            Departure: 12,
        },
        {
            Id: 1,
            UserId: "",
            Name: "Paris",
            Departure: 12,
        }
    ];

    constructor(
        private http: HttpClient
    ) { }

    public getTimetableConnections(): Observable<Array<TimetableConnection>> {
        return this.http.get<Array<TimetableConnection>>(this.url);
    }

    public deleteTimeTableConnection(id: number): Observable<any> {
        return this.http.delete<void>(this.url + `/${id}`);
    }

    public postTimeTableConnection(destination: string, time: number): Observable<any> {
        const maxId = Math.max.apply(Math, this.timeTableMock.map(function(item) { return item.Id; }));
        const id = maxId + 1;
        console.warn("ID", id)
        return this.http.post<void>(this.url, {
            "Id": id,
            "UserId": "admin",
            "Name": destination,
            "Departure": time
          });
    }
}