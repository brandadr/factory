import { Injectable } from "@angular/core";
import { BehaviorSubject, combineLatest, Observable, of, timer } from "rxjs";
import { map } from "rxjs/operators";
import { TimetableConnection, TimetableConnectionDisplay } from "../interfaces/timetable-connection";
import { ITimetableDataService } from "../interfaces/timetable-data-service";
import { TimetableMockDataService } from "./timetable-mock.data-service";
import { TimetableDataService } from "./timetable.data-service";

@Injectable({
    providedIn: 'root'
})
export class TimetableDomainService {
    private readonly isEditMode = new BehaviorSubject<boolean>(false);
    public isEditMode$ = this.isEditMode.asObservable();

    private readonly isDemoMode = new BehaviorSubject<boolean>(false);
    public isDemoMode$ = this.isDemoMode.asObservable();

    private readonly timeTable = new BehaviorSubject<Array<TimetableConnection>>([]);
    public timeTable$ = this.timeTable.asObservable();

    public timerSeconds$: Observable<number> = of(0);
    public currentTime$: Observable<Date> = of(new Date());
    public nextDeparture$: Observable<TimetableConnectionDisplay> = of({ Name: '', Departure: new Date() });
    public remainingSecondsToNextDeparture$: Observable<number> = of(0);

    public currentTimetableDataService: ITimetableDataService = this.timetableDataService;

    constructor(
        private timetableDataService: TimetableDataService,
        private timetableMockDataService: TimetableMockDataService
    ) { }

    public init(): void {
        this.loadConnections();
        this.timerSeconds$ = timer(0, 1000);
        this.currentTime$ = this.timerSeconds$.pipe(
            map(() => {
              return new Date();
            }));
        
        this.nextDeparture$ = combineLatest([this.currentTime$, this.timeTable$]).pipe(
            map(([currentTime, timeTable]) => {
          
                return this.getNextDeparture(currentTime, timeTable);
            }));
          
        this.remainingSecondsToNextDeparture$ = combineLatest([this.currentTime$, this.nextDeparture$]).pipe(
            map(([currentTime, nextDeparture]) => {
          
                if (nextDeparture.Departure) {
                    return Math.floor((nextDeparture.Departure?.getTime() - currentTime.getTime()) / 1000);
                }

                return 0;
            }));
    }

    public loadConnections(): void {
        this.timeTable$ = this.currentTimetableDataService.getTimetableConnections();
    }

    public deleteConnection(id: number): void {
        this.currentTimetableDataService.deleteTimeTableConnection(id).subscribe(() => this.init());
    }

    public addConnection(destination: string, time: number): void {
        this.currentTimetableDataService.postTimeTableConnection(destination, time).subscribe(() => this.init());
    }

    public setEditMode(isEditMode: boolean) {
        this.isEditMode.next(isEditMode);
    }

    public setDemoMode(isDemoMode: boolean) {
        isDemoMode ? this.currentTimetableDataService = this.timetableMockDataService : this.currentTimetableDataService = this.timetableDataService;
        this.isDemoMode.next(isDemoMode);
        this.init();
    }

    private getNextDeparture(currentTime: Date, timeTable: TimetableConnection[]): TimetableConnectionDisplay {
        if (timeTable?.length < 1) {
            return { Name: '', Departure: null };
        }

        const timeTableSortedAsc = timeTable.sort((a, b) => a.Departure - b.Departure);
        const minutesNow = currentTime.getMinutes();
        const hoursNow = currentTime.getHours();

        for (let item of timeTableSortedAsc) {
            if (minutesNow < item.Departure) {
                return { Name: item.Name, Departure: new Date(currentTime.setMinutes(item.Departure, 0)) };
            }
        }

        return { Name: timeTableSortedAsc[0].Name, Departure: new Date(currentTime.setHours(hoursNow + 1, timeTableSortedAsc[0].Departure, 0)) };
    }
}