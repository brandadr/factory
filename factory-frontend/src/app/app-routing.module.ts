import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoComponent } from './info/info.component';
import { GardenManagerDevicesComponent } from './garden-manager-devices/garden-manager-devices.component';
import { FactoryUiDemoComponent } from './factory-ui-demo/factory-ui-demo.component';
import { TimetableComponent } from './timetable/timetable.component';
import { CameraComponent } from 'projects/camera/src/public-api';
import { GardenManagerLogsComponent } from './garden-manager-logs/garden-manager-logs.component';
import { DecisionFinderComponent } from './decision-finder/decision-finder.component';
import { RunningEventsPageComponent } from 'projects/running-events/src/public-api';

const routes: Routes = [
  {
    path: 'info',
    component: InfoComponent,
    pathMatch: 'full'
  },
  {
    path: 'timetable',
    component: TimetableComponent,
  },
  {
    path: 'running-events',
    component: RunningEventsPageComponent,
  },
  {
    path: 'decision-finder',
    component: DecisionFinderComponent,
  },
  {
    path: 'garden-manager-logs',
    component: GardenManagerLogsComponent,
  },
  {
    path: 'garden-manager-devices',
    component: GardenManagerDevicesComponent,
  },
  {
    path: 'camera',
    component: CameraComponent,
  },
  {
    path: 'factory-ui-demo',
    component: FactoryUiDemoComponent,
  },
  {
    path: '',
    component: InfoComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
