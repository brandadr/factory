import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CameraComponent } from './camera/camera.component';
import { FactoryUiDemoComponent } from './factory-ui-demo/factory-ui-demo.component';
import { InfoComponent } from './info/info.component';
import { TimetableComponent } from './timetable/timetable.component';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { GardenManagerDevicesComponent } from './garden-manager-devices/garden-manager-devices.component';
import { TimetableAssistantModule, TimetableAssistantOverviewComponent } from 'projects/timetable-assistant/src/public-api';
import { CameraModule } from 'projects/camera/src/public-api';
import { FactoryUiLibModule } from 'factory-ui';
import { DeviceManagerDemoComponent } from './factory-ui-demo/device-manager-demo/device-manager-demo.component';
import { GardenManagerLogsComponent } from './garden-manager-logs/garden-manager-logs.component';
import { DecisionFinderComponent } from './decision-finder/decision-finder.component';
import { DecisionFinderModule } from 'projects/decision-finder/src/projects';

@NgModule({
  declarations: [
    AppComponent,
    CameraComponent,
    FactoryUiDemoComponent,
    GardenManagerDevicesComponent,
    GardenManagerLogsComponent,
    InfoComponent,
    TimetableComponent,
    DeviceManagerDemoComponent,
    GardenManagerLogsComponent,
    DecisionFinderComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    CameraModule,
    FactoryUiLibModule,
    FlexLayoutModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatProgressBarModule,
    MatTableModule,
    MatDialogModule,
    TimetableAssistantModule,
    DecisionFinderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
