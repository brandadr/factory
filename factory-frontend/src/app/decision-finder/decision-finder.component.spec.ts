import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DecisionFinderComponent } from './decision-finder.component';

describe('DecisionFinderComponent', () => {
  let component: DecisionFinderComponent;
  let fixture: ComponentFixture<DecisionFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DecisionFinderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DecisionFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
