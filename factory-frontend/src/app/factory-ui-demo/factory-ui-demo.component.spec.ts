import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FactoryUiDemoComponent } from './factory-ui-demo.component';

describe('FactoryUiDemoComponent', () => {
  let component: FactoryUiDemoComponent;
  let fixture: ComponentFixture<FactoryUiDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FactoryUiDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FactoryUiDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
