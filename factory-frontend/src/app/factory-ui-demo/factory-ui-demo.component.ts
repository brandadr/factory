import { Component } from '@angular/core';
import { GenericDialogField, TableColumnDefinition, TableColumnType } from 'factory-ui';
import { GenericDialogService } from 'factory-ui';

@Component({
  selector: 'app-factory-ui-demo',
  templateUrl: './factory-ui-demo.component.html',
  styleUrls: ['./factory-ui-demo.component.scss']
})
export class FactoryUiDemoComponent {

  public columnDefinitions: Array<TableColumnDefinition> = [
    {id: 'name', description: 'Name', type: TableColumnType.Text},
    {id: 'age', description: 'Alter', type: TableColumnType.Text},
    {id: 'address', description: 'Adresse', type: TableColumnType.Text},
  ];
  public tableData: Array<any> = [
    {name: 'Thomas Meier', age: '42', address: 'Bahnhofstrasse 10, 9000 St.Gallen'},
    {name: 'Fritz Muster', age: '31', address: 'Oberstrasse 106, 9000 St.Gallen'},
    {name: 'John Travolta', age: '19', address: 'Dorfweg 1, 8000 Zürich'},
  ];

  public dialogValues: Array<GenericDialogField> = [
    {id: 'field1', labelName: 'Feld 1', value: "Field 1"},
    {id: 'field2', labelName: 'Feld 2', value: "Field 2"},
  ];
  public columnDefinitionIds: any =this.columnDefinitions.map(item => item.id);

  constructor(
    public readonly genericDialogService: GenericDialogService
  ) {

   }

  public onOpenGenericDialog(): void {

    this.genericDialogService.openDialog(     
      "Sample Title",
      "Description of what will happen in this dialog.",
      this.dialogValues,
      () => {
        console.warn("Dialog has been closed.", this.dialogValues)
      });
  }
}
