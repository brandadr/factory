import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GardenManagerDevicesComponent } from './garden-manager-devices.component';

describe('GardenManagerDevicesComponent', () => {
  let component: GardenManagerDevicesComponent;
  let fixture: ComponentFixture<GardenManagerDevicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GardenManagerDevicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GardenManagerDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
