import { Component, OnInit } from '@angular/core';
import { DeviceCommand, DeviceManagerItem } from 'factory-ui';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { Subject } from 'rxjs/internal/Subject';
import { map, tap } from 'rxjs/operators';
import { DeviceManagerService } from '../services/device-manager.service';

@Component({
  selector: 'app-garden-manager-devices',
  templateUrl: './garden-manager-devices.component.html',
  styleUrls: ['./garden-manager-devices.component.scss']
})

export class GardenManagerDevicesComponent implements OnInit {
  public deviceManagerItems$: Observable<Array<DeviceManagerItem>> = of([]);
  public commandExecute: Subject<boolean> = new Subject();
  public commandExecute$: Observable<boolean> = this.commandExecute.asObservable();

  constructor(
    public readonly deviceManagerService: DeviceManagerService
  ) { }

  public ngOnInit(): void {
    this.load();
  }

  private load(): void {
    this.deviceManagerItems$ = this.deviceManagerService.getDevices().pipe(map(devices => {
      let items = [];

      for (let device of devices) {
        let deviceManagerItem: DeviceManagerItem = {
          Device: device,
          DeviceInformation: this.deviceManagerService.getDeviceInformationById(device.Id).pipe(map(deviceInformation => deviceInformation)),
          IsReadOnly: false,
          IsLoading: false,
          executeCommand: (command: DeviceCommand) => this.deviceManagerService.writeDevice(command), 
          getDeviceInformation: () => this.deviceManagerService.getDeviceInformationById(device.Id).pipe(map(deviceInformation => deviceInformation))
        }

        items.push(deviceManagerItem)
      }

      return items;
    }));
  }
}