import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GardenManagerLogsComponent } from './garden-manager-logs.component';

describe('GardenManagerLogsComponent', () => {
  let component: GardenManagerLogsComponent;
  let fixture: ComponentFixture<GardenManagerLogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GardenManagerLogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GardenManagerLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
