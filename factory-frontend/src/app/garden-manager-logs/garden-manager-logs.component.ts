import { Component, OnInit } from '@angular/core';
import { TableColumnType } from 'factory-ui';
import { map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { DeviceManagerService } from '../services/device-manager.service';

@Component({
  selector: 'app-garden-manager-logs',
  templateUrl: './garden-manager-logs.component.html',
  styleUrls: ['./garden-manager-logs.component.scss']
})
export class GardenManagerLogsComponent implements OnInit {
  public columnDefinitions: Array<any> = [
    {id: 'id', description: 'Id', type: TableColumnType.Text},
    {id: 'content', description: 'Inhalt', type: TableColumnType.Text}
  ];
  public tableData$: Observable<Array<any>> = of([{id: "", content: "", date: ""}]);


  constructor(
    public readonly deviceManagerService: DeviceManagerService
  ) { }

  public ngOnInit(): void {
    this.tableData$ = this.deviceManagerService.getLogs().pipe(
      map((logs: Array<string>) => 
        {
          let logItems = [];
          let index = 0; 
          for(let log of logs) {
            logItems.push({id: index++, content: log});
          }

          return logItems;
        }
      )
    );
  }

}
