import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Device, DeviceCommand, DeviceInformation } from "factory-ui";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class DeviceManagerService {

    private readonly url: string = `${environment.backendUrl}/api/gardenmanager/devices`;
    private readonly logsUrl: string = `${environment.backendUrl}/api/gardenmanager/logs`;

    constructor(private http: HttpClient) { }

    public getDevices(): Observable<Array<Device>> {

        return this.http.get<Array<Device>>(this.url);
    }

    public writeDevice(deviceCommand: DeviceCommand): Observable<any> {

        return this.http.put<any>(`${this.url}/update` , deviceCommand);
    }

    public getDeviceInformation(): Observable<Array<DeviceInformation>> {

        return this.http.get<Array<DeviceInformation>>(`${this.url}/information`);
    }

    public getDeviceInformationById(id: number): Observable<DeviceInformation> {

        return this.http.get<DeviceInformation>(`${this.url}/information/${id}`);
    }

    public getLogs(): Observable<Array<string>> {
        return this.http.get<Array<string>>(`${this.logsUrl}`);
    }
}