export const environment = {
  production: true,
  backendUrl: 'https://factory-api.brandadrian.ch',
  stravaAuthUrl: "https://www.strava.com/oauth/authorize?client_id=34855&response_type=code&redirect_uri=https://factory.brandadrian.ch/running-events&approval_prompt=force&scope=activity:read_all"
};
