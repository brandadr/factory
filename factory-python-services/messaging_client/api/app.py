from flask import Flask, jsonify, render_template
from redis import Redis
import pika, os
from datetime import datetime

app = Flask(__name__)
redis = Redis(host='redis', port=6379)

@app.route('/')
def home():
    result = []
    members = redis.smembers('messageQueueHistory')
    for member in members:
        memberString = str(member)
        result.append(memberString[2:])
    messages = jsonify(result)
    return render_template('index.html', messages=result)
    
@app.route('/test')
def test():
    messages1 = ['test', 'test2']
    return render_template('index.html', messages1=messages1)

@app.route('/status')
def info():
    return 'queue: factory-queue-python, ip: 192.168.1.186'

@app.route('/messages')
def messages():
    result = []
    members = redis.smembers('messageQueueHistory')
    for member in members:
        memberString = str(member)
        result.append(memberString[2:])

    return jsonify(result)

@app.route('/send')
def send():
    msg_client_pwd=os.environ["RABBITMQ_PWD"]
    msg_client_usr=os.environ["RABBITMQ_USR"]
    msg_client_ip=os.environ["RABBITMQ_IP"]
    msg_client_port=os.environ["RABBITMQ_PORT"]
    credentials = pika.PlainCredentials(msg_client_usr, msg_client_pwd)
    parameters = pika.ConnectionParameters(msg_client_ip, msg_client_port, '/', credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='factory-queue-python', durable=True)
    
    now = datetime.now()
    date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
    message = 'Message sent at ' + date_time
    
    channel.basic_publish(exchange='',routing_key='factory-queue-python', body=message)
    connection.close()

    return 'message sent'

if __name__ == "__main__":
    app.run(host="0.0.0.0")
