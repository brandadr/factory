#!/usr/bin/env python
import pika, sys, os
from redis import Redis

redis = Redis(host='redis', port=6379)

def main():
    msg_client_pwd=os.environ["RABBITMQ_PWD"]
    msg_client_usr=os.environ["RABBITMQ_USR"]
    msg_client_ip=os.environ["RABBITMQ_IP"]
    msg_client_port=os.environ["RABBITMQ_PORT"]
    credentials = pika.PlainCredentials(msg_client_usr, msg_client_pwd)
    parameters = pika.ConnectionParameters(msg_client_ip, msg_client_port, '/', credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='factory-queue-python', durable=True)

    def callback(ch, method, properties, body):
        redis.sadd('messageQueueHistory', body)

    channel.basic_consume(queue='factory-queue-python', on_message_callback=callback, auto_ack=True)
    channel.start_consuming()

if __name__ == '__main__':
    main()
