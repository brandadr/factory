from miflora.miflora_poller import MiFloraPoller
from btlewrap.gatttool import GatttoolBackend

sensor = MiFloraPoller('80:EA:CA:89:34:A5', GatttoolBackend)

temperature = "Temperatur: " + str(sensor.parameter_value('temperature'))
light = ";Licht: " + str(sensor.parameter_value('light'))
moisture = ";Feuchtigkeit: " + str(sensor.parameter_value('moisture'))
conductivity = ";Leitfähigkeit: " + str(sensor.parameter_value('conductivity'))
battery = ";Batterie: " + str(sensor.parameter_value('battery'))
    
print(temperature + light + moisture + conductivity + battery)